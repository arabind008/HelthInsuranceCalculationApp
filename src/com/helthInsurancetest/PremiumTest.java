package com.helthInsurancetest;

import org.junit.Assert;
import org.junit.Test;

import com.helthInsurance.models.Person;
import com.helthInsurance.services.PremiumCalculationServices;

public class PremiumTest {
	
	@Test
	public void test() {
		Person person = new Person();
		person.setFirstName("Norman");
		person.setLastName("Gomes");
		person.setAge(34);
		person.setGender('m');
		person.setHypertension(false);
		person.setBloodPressure(false);
		person.setBloodSugar(false);
		person.setOverweight(true);
		person.setSmoking(false);
		person.setAlcohol(true);
		person.setDailyExercise(true);
		person.setDrugs(false);
		
		int premium = PremiumCalculationServices.premiumCalculation(person);
		Assert.assertEquals(6856, premium);
		person.setPremium(premium);
		
		String outputString =PremiumCalculationServices.getOutputString(person);
		Assert.assertEquals("Health Insurance Premium for Mr. Gomes: Rs. 6,856", outputString);
	}


}
