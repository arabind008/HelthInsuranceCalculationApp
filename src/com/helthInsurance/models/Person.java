package com.helthInsurance.models;

public class Person {
	

	private String firstName;
	private String lastName;
	private char gender = 'f';
	private int age;
	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overweight;
	private boolean smoking;
	private boolean alcohol;
	private boolean dailyExercise;
	private boolean drugs;
	private int premium;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverweight() {
		return overweight;
	}
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	public int getPremium() {
		return premium;
	}
	public void setPremium(int premium) {
		this.premium = premium;
	}
	
	
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + (alcohol ? 1231 : 1237);
		result = prime * result + (bloodPressure ? 1231 : 1237);
		result = prime * result + (bloodSugar ? 1231 : 1237);
		result = prime * result + (dailyExercise ? 1231 : 1237);
		result = prime * result + (drugs ? 1231 : 1237);
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + gender;
		result = prime * result + (hypertension ? 1231 : 1237);
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + (overweight ? 1231 : 1237);
		result = prime * result + premium;
		result = prime * result + (smoking ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (alcohol != other.alcohol)
			return false;
		if (bloodPressure != other.bloodPressure)
			return false;
		if (bloodSugar != other.bloodSugar)
			return false;
		if (dailyExercise != other.dailyExercise)
			return false;
		if (drugs != other.drugs)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender != other.gender)
			return false;
		if (hypertension != other.hypertension)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (overweight != other.overweight)
			return false;
		if (premium != other.premium)
			return false;
		if (smoking != other.smoking)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName
				+ ", gender=" + gender + ", age=" + age + ", hypertension="
				+ hypertension + ", bloodPressure=" + bloodPressure
				+ ", bloodSugar=" + bloodSugar + ", overweight=" + overweight
				+ ", smoking=" + smoking + ", alcohol=" + alcohol
				+ ", dailyExercise=" + dailyExercise + ", drugs=" + drugs
				+ ", premium=" + premium + "]";
	}
	
	
	


}
