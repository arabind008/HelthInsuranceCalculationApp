package com.helthInsurance.services;

import java.text.NumberFormat;

import com.helthInsurance.models.Person;

public class PremiumCalculationServices {
	public static int premiumCalculation(Person person) {
	
	
	double result = 5000d;
	
	if(person.getAge() >=18){
		result +=result * 10/100;
	}
	
	//% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
	if(person.getAge() >=25){
		if(person.getAge() < 41){
			int i = 25;
			while (i <= person.getAge()) {
				result += result * 10 / 100;
				i = i + 5;
			}
		}else{
			int i = 41;
			while (i <= person.getAge()) {
				result += result * 20 / 100;
				i = i + 5;
			}
		}
	}
	
			
	if (person.getGender() == 'm') {
		result += result * 2 / 100;
	}
	
	
	int preexisting = 0;
	if (person.isHypertension()) {
		preexisting++;
	}
	
	if (person.isBloodPressure()) {
		preexisting++;
	}
	
	if (person.isBloodSugar()) {
		preexisting++;
	}
	
	if (person.isOverweight()) {
		preexisting++;
	}
	
	result += result * preexisting / 100;
	
	int habits = 0;
	//Good habits (Daily exercise) -> Reduce 3% for every good habit
	if (person.isDailyExercise()) {
		habits -= 3;
	}
	
	//Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
	if (person.isSmoking()) {
		habits += 3;
	}
	
	if (person.isAlcohol()) {
		habits += 3;
	}
	
	if (person.isDrugs()) {
		habits += 3;
	}
	
	result += result * habits / 100;
	
	return (int) Math.round(result);
}

public static String getOutputString(Person person) {
	String title = person.getGender() == 'm' ? "Mr. " : "Ms. ";
	StringBuffer output = new StringBuffer();
	output.append("Health Insurance Premium for ").append(title).append(person.getLastName()).append(": Rs. ")
		.append(NumberFormat.getIntegerInstance().format(person.getPremium()));

	return output.toString();
}


}
